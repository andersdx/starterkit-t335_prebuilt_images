================================================
	Anders DX1 Pre-Built SW Images
================================================
		= RELEASE NOTES =



> DX1 Release Package v1.1 <

- Features Added:
	1. Debian Stable Branch Release Update (v.7.11 - Wheezy)
	2. Rootfs Size optimized for 512MB eMMC devices
	3. QT pre-built libraries updated to v.5.8


- Fixes
	None


- Known Issues:
	1. Slow boot-up time






> DX1 Release Package v1.1 <

- Features Added:
	1. 4.3", 7.0" CTP Anders display support
	2. Anders CTP touchsceen driver support
	3. QT Libraries


- Fixes
	1. Installing script fixes.


- Known Issues:
	1. Slow boot-up time
