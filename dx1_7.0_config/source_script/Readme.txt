Anders scripts generation instruction:

1. Edit the source file present in "source_script" folder
2. Enter the "source_script" folder
3. run command:
	sudo mkimage -A arm -T script -C none -n "DX1b 7.0 boot script" -d ./dx1b_boot_7.0.script ../boot.scr
4. Place the output boot.scr file in the SD card along with the rest of the images.
